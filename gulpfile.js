var gulp = require('gulp'),
    sass =  require('gulp-sass'),
    browserSync = require('browser-sync'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglifyjs'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    imagemin = require('gulp-imagemin'),
    rigger = require('gulp-rigger');

gulp.task('sass', () =>{
    return gulp.src('app/sass/style.scss')
    .pipe(sass())
    .pipe(concat('style.css'))
    .pipe(gulp.dest('build/css'))
    .pipe(browserSync.stream());
});

gulp.task('bootstrap', () =>{
    return gulp.src('app/sass/bootstrap/*.scss')
    .pipe(sass())
    .pipe(concat('bootstrap.css'))
    .pipe(gulp.dest('build/css'))
    .pipe(browserSync.stream());
});

gulp.task('bootstrap-select', () =>{
    return gulp.src('app/sass/bootstrap-select/*.scss')
    .pipe(sass())
    .pipe(concat('bootstrap-select.css'))
    .pipe(gulp.dest('build/css'))
    .pipe(browserSync.stream());
});

gulp.task('html-rigger', function(){
  return gulp.src(['app/**/*.html'])
    .pipe(rigger())
    .pipe(gulp.dest('build/'))
    .pipe(browserSync.stream());
});

gulp.task('serve', () =>{
    browserSync.init({
        server: {
            baseDir: 'build'
        }
    });
    gulp.watch('app/sass/*.scss', ['sass']);
    gulp.watch('app/**/*.html', ['html-rigger']).on('change', browserSync.reload);
});

gulp.task('watch', ['sass', 'serve', 'bootstrap', 'bootstrap-select', 'html-rigger']);